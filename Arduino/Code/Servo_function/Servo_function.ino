#include <Servo.h>

Servo myservo;

int Input = A0;  
int Output = 9; 

int sensorValue = 0;        

void setup() {
  Serial.begin(9600);
      myservo.attach(8);

}

void loop() {
  sensorValue = analogRead(Input);
  digitalWrite(Output, HIGH);

  Serial.print("sensor = ");
  Serial.println(sensorValue);

  if((sensorValue >=980) && (sensorValue<=1010)){
        myservo.write(45);
  }
  else{
    myservo.write(90);

  }
  delay(100);
  
}
